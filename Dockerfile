
############################################################
# Dockerfile build Go
# in ubuntu
############################################################
# Base image to ubuntu:20.04
FROM ubuntu:20.04

# Install.
RUN \
    apt-get update && \
    apt-get -y upgrade && \
    DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata && \
    apt-get install -y build-essential && \
    apt-get install -y software-properties-common && \
    apt-get install -y curl git vim autossh rsync screen && \
    rm -rf /var/lib/apt/lists/*

RUN apt-get update

#Install SSHD
RUN apt-get install -y openssh-server openssh-client && \
    mkdir /var/run/sshd

# Install Go
RUN \
    mkdir /usr/local/go && \
    curl https://storage.googleapis.com/golang/go1.15.5.linux-amd64.tar.gz | tar xvzf - -C /usr/local/go --strip-components=1 && \
    mkdir /go

# Define working directory.
WORKDIR /root

# Set environment variables.
ENV HOME /root
ENV GOPATH /go
ENV GOROOT /usr/local/go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH

RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"

#Add file from dir
COPY ./start.sh /root
COPY ./sshd_config /etc/ssh/sshd_config

#Change Access
RUN chmod u+x /root/start.sh

#Open Port
EXPOSE 22

# Define default command.
CMD ["/bin/bash", "/root/start.sh"]