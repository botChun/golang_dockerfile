Clone Dockerfile

    git clone https://botChun@bitbucket.org/botChun/golang_dockerfile.git


Build Image

    cd golang_dockerfile
    docker build -t golang:1.15.5 .



Build Container

    docker run -v /home/.go:/home/.go -v /home/.logs:/root/.logs -d -p :22 --name golang golang:1.15.5 /root/start.sh